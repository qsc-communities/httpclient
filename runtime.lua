-- Aliases
Status = Controls.Status
SendGet = Controls.SendGet
SendPost = Controls.SendPost
SendPut = Controls.SendPut
ResponseText = Controls.ResponseText

-- Constants
StatusState = { OK = 0, COMPROMISED = 1, FAULT = 2, NOTPRESENT = 3, MISSING = 4, INITIALIZING = 5}  -- Status states in designer

-- Variables
RequestTimeout = 10            -- Timeout of the connection in seconds
Host = "https://httpbin.org"   -- HTTP server host name or IP address to use for connection
IPAddress = "3.211.1.78"       -- IP address can be for host
Path = "put"                   -- Path within the site to access
Port = 443                     -- Port to use (if not 80 or 443)
Username = "admin"             -- Login Data for the http server
Password = "password"          -- Login Data for the http server
QueryData = {                  -- Some Data for the http server
  ["Hello"] = "World", 
  ["Value1"] = 1, 
  ["Ancilliary Data"] = "users/use-weird*characters."
}
PostData = "{'Hello':'world', 'Value1':1}"

--Debug level
DebugTx,DebugRx,DebugFunction = false, false, false
DebugPrint = Properties["Debug Print"].Value
if DebugPrint=="Tx/Rx" then
  DebugTx,DebugRx=true,true
elseif DebugPrint=="Tx" then
  DebugTx=true
elseif DebugPrint=="Rx" then
  DebugRx=true
elseif DebugPrint=="Function Calls" then
  DebugFunction=true
elseif DebugPrint=="All" then
  DebugTx,DebugRx,DebugFunction=true,true,true
end

-- Functions
-- Function that sets plugin status
function ReportStatus(state, msg)
  if DebugFunction then print("ReportStatus() called:" .. state) end
  local msg = msg or ""
  Status.Value = StatusState[state]  -- Sets status state
  Status.String = msg  -- Sets status message
end

-- Function reads response code, sets status and prints received data.
function ResponseHandler(tbl, code, data, err, headers)
  if DebugFunction then print("HTTP Response Handler called") end
  if DebugRx then print("HTTP Response Code: " .. code) end
  if code == 200 then  -- Vaild response
    ReportStatus("OK")
    if DebugRx then print("Rx: ", data) end
    ResponseText.String = data

  elseif code == 401.0 or IPAddress.String == "" then  -- Invalid Address handler
    ReportStatus("MISSING", "Check TCP connection properties") 

  else   -- Other error cases
    ReportStatus("FAULT", err) 
  end
end

-- Send an HTTP GET request to the defined
function GetRequest()
  if DebugFunction then print("GetRequest() called") end
  -- Define any HTTP headers to sent
  headers = {
    ["Content-Type"] = "text/html",
    ["Accept"] = "text/html"
  }
  -- Generate the URL of the request using HTTPClient formatter
  url = HttpClient.CreateUrl({
    ["Host"] = Host,
    ["Port"] = Port,
    ["Path"] = "get",
    ["Query"] = QueryData
  })

  if DebugTx then print("Sending GET request: " .. url) end
  HttpClient.Download({ 
    Url          = url,
    Method       = "GET",
    Headers      = headers,
    User         = Username,  -- Only needed if device requires a sign in
    Password     = Password,  -- Only needed if device requires a sign in
    Timeout      = RequestTimeout,
    EventHandler = ResponseHandler
  })
end

-- Send a POST request to the HTTP server
function PostRequest()
  if DebugFunction then print("PostRequest() called") end
  -- Define any HTTP headers to sent
  headers = {  
    ["Content-Type"] = "text/html",
    ["Accept"] = "text/html"
  }
  -- Generate the URL of the request using HTTPClient formatter
  url = HttpClient.CreateUrl({
    ["Host"] = Host,
    ["Path"] = "post"
  })

  if DebugTx then print("Sending POST request to: " .. url, PostData) end
  HttpClient.Upload({ 
    Url          = url,
    Headers      = headers,
    User         = Username,  -- Only needed if device requires a sign in
    Password     = Password,  -- Only needed if device requires a sign in
    Data         = PostData,
    Method       = "POST",
    Timeout      = RequestTimeout,
    EventHandler = ResponseHandler
  })
end

-- Generate a simple put request to an IP Address
function PutRequest()
  if DebugFunction then print("PutRequest() called") end
  -- Use the HTTPClient Encoder to format the PostData into an HTTP sendable string (Netscape encoding)
  data = HttpClient.EncodeString(PostData)

  if DebugTx then print("Sending PUT request to: " .. string.format("http://%s/%s", IPAddress, Path), data) end
  HttpClient.Upload({ 
    Url          = string.format("http://%s/%s", IPAddress, Path), -- This format is an example and is not standard for all devices
    Data         = data,
    Method       = "PUT",
    Timeout      = RequestTimeout,
    EventHandler = ResponseHandler
  })
end

-- Control Event Handler Binding
SendGet.EventHandler = GetRequest  
SendPost.EventHandler = PostRequest 
SendPut.EventHandler = PutRequest 
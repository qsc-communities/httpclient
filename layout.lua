-- Text display
table.insert(graphics, {
  Type = "Header",
  Text = "HTTP Client Command Test",
  Position = {5, 5},
  Size = {295, 16},
  FontSize = 14,
  HTextAlign = "Center"
})
table.insert(graphics, {
  Type = "Text",
  Text = "Send GET",
  Position = {5, 25},
  Size = {95, 16},
  FontSize = 14,
  HTextAlign = "Right"
})
table.insert(graphics, {
  Type = "Text",
  Text = "Send POST",
  Position = {5, 45},
  Size = {95, 16},
  FontSize = 14,
  HTextAlign = "Right"
})
table.insert(graphics, {
  Type = "Text",
  Text = "Send PUT",
  Position = {5, 65},
  Size = {95, 16},
  FontSize = 14,
  HTextAlign = "Right"
})
table.insert(graphics, {
  Type = "Text",
  Text = "Status",
  Position = {5, 85},
  Size = {95, 16},
  FontSize = 14,
  HTextAlign = "Right"
})
table.insert(graphics, {
  Type = "Text",
  Text = "Response",
  Position = {5, 105},
  Size = {95, 16},
  FontSize = 14,
  HTextAlign = "Right"
})

--Controls
layout["SendGet"] = {
  PrettyName = "Send GET Request",
  Legend = "Send",
  FontSize = 12,
  Style = "Button",
  Position = {105, 25},
  Size = {40, 16}
}
layout["SendPost"] = {
  PrettyName = "Send POST Request",
  Legend = "Send",
  FontSize = 12,
  Style = "Button",
  Position = {105, 45},
  Size = {40, 16}
}
layout["SendPut"] = {
  PrettyName = "Send PUT Request",
  Legend = "Send",
  FontSize = 12,
  Style = "Button",
  Position = {105, 65},
  Size = {40, 16}
}
layout["Status"] = {
  PrettyName = "Connection Status", 
  Position = {105, 85}, 
  Size = {200, 16}
}
layout["ResponseText"] = {
  PrettyName = "Data",
  Position = {105, 105}, 
  Size = {200, 64},
  HTextAlign = "Left"
}